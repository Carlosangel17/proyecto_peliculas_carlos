<?php

use Illuminate\Database\Seeder;
use App\Pelicula;
use App\Resena;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        self::seedPeliculas();
        $this->command->info("tabla peliculas inicializada con datos");
        self::seedResenas();
        $this->command->info("tabla resenas inicializada con datos");
        
    }

    public function seedPeliculas(){
    	DB::table("peliculas")->delete();

    	$pelicula = new Pelicula();
    	$pelicula->titulo = "Star Wars: The Rise of Skywalker";
    	$pelicula->year = 2019;
    	$pelicula->duracion = 141;
    	$pelicula->pais = "Estados Unidos";
    	$pelicula->direccion = "J.J. Abrams";
    	$pelicula->guion = "J.J. Abrams, Chris Terrio";
    	$pelicula->musica = "John Williams";
    	$pelicula->fotografia = "Daniel Mindel";
    	$pelicula->reparto = "     Daisy Ridley, Adam Driver, John Boyega, Oscar Isaac, Kelly Marie Tran, Joonas Suotamo, Domhnall Gleeson, Ian McDiarmid, Carrie Fisher, Anthony Daniels, Keri Russell, Billie Lourd, Lupita Nyong'o, Billy Dee Williams, Naomi Ackie, Richard E. Grant, Dominic Monaghan, Freddie Prinze Jr., Greg Grunberg, Jimmy Vee, Denis Lawson, Richard Bremmer, Amir El-Masry, Dave Chapman, Harrison Ford, Mark Hamill, Nasser Memarzia, Simon Paisley Day, Brian Herring, Philicia Saunders, Lin-Manuel Miranda, Jodie Comer, Billy Howle, Warwick Davis, Cailey Fleming, Ann Firbank, John Williams ";
    	$pelicula->productora = "Lucasfilm / Bad Robot / Walt Disney Pictures. Distribuida por Walt Disney Pictures";
    	$pelicula->genero = "Ciencia ficción. Aventuras. Fantástico. Acción";
    	$pelicula->sinopsis = "Un año después de los eventos de 'Los últimos Jedi', los restos de la Resistencia se enfrentarán una vez más a la Primera Orden, involucrando conflictos del pasado y del presente. Mientras tanto, el antiguo conflicto entre los Jedi y los Sith llegará a su clímax, lo que llevará a la saga de los Skywalker a un final definitivo. Final de la trilogía iniciada con 'El despertar de la Fuerza'.";
    	$pelicula->foto = "starWarsRotS.jpg";
   		$pelicula->save();
    }

    public function seedResenas(){
        DB::table("resenas")->delete();
        $pelicula = Pelicula::all()->first()->id;
        $resena = new Resena();

        $resena->pelicula_id = $pelicula;
        $resena->critica = "Es una basura";
        $resena->valoracion = 0;
        $resena->save();
    }
}
