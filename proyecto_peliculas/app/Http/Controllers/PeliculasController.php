<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelicula;
use App\Resena;
use Illuminate\Support\Facades\Storage;

class PeliculasController extends Controller
{
    public function getPeliculas(){
    	$peliculas = Pelicula::all();
    	return view("peliculas.index", array("peliculas" =>$peliculas));
    }

    public function getInformacion($id){
    	$pelicula = pelicula::findOrFail($id);
    	return view("peliculas.informacion", array("pelicula" => $pelicula, "id"=>$pelicula->id) );
    }

    public function getCrear(){
    	return view("peliculas.crear");
    }

    public function postCrear(Request $request){
        $pelicula = new Pelicula();
        $pelicula->titulo = $request->titulo;
        $pelicula->year = $request->año;
        $pelicula->duracion = $request->duracion;
        $pelicula->pais = $request->pais;
        $pelicula->direccion = $request->direccion;
        $pelicula->guion = $request->guion;
        $pelicula->musica = $request->musica;
        $pelicula->fotografia = $request->fotografia;
        $pelicula->reparto = $request->reparto;
        $pelicula->productora = $request->productora;
        $pelicula->genero = $request->genero;
        $pelicula->sinopsis = $request->sinopsis;
        $pelicula->foto = $request->foto->store("", "peliculas");
        $pelicula->save();
        return redirect("peliculas")->with("mensaje", "pelicula creada");
    }

    public function getCrearResena($id){
    	$pelicula = pelicula::findOrFail($id);
    	return view("peliculas.crearResena", array("pelicula" => $pelicula));
    }

    public function postCrearResena($id, Request $request){
        $resena = new Resena();
        $resena->pelicula_id = $id;
        $resena->critica = $request->critica;
        $resena->valoracion = $request->valoracion;
        $resena->save();
        return redirect("peliculas/resenas/$id")->with("mensaje", "reseña creada");
    }



    public function getResenas($id){
        $pelicula = pelicula::findOrFail($id);
        return view("peliculas.resenas", array("pelicula" => $pelicula));
    }
}
