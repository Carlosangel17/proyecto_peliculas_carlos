<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    public function getResenas(){
    	return $this->hasMany("App\Resena");
    }
}
