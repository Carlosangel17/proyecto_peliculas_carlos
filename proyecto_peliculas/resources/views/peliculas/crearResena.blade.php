@extends("layouts.master")

@section("titulo")
Añadir reseña
@endsection
@section("contenido")
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">Añadir reseña</div>
			<div class="card-body" style="padding:30px">
				<form action="{{ url('peliculas/crearResena') }}/{{$pelicula->id}}" method="POST" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group">
						<label for="critica">Critica</label>
						<textarea name="critica" id="critica" class="form-control" rows="3">

						</textarea>
					</div>
					<div class="form-group">
						<label for="valoracion">Valoracion</label>
						<input type="number" name="valoracion" id="valoracion" min="0" max="10" class="form-control">
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Enviar reseña</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection