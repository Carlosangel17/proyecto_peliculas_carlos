@extends("layouts.master")

@section("titulo")
Añadir pelicula
@endsection
@section("contenido")
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">Añadir pelicula</div>
			<div class="card-body" style="padding:30px">
				<form action="{{ action('PeliculasController@postCrear') }}" method="POST" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group">
						<label for="titulo">Titulo</label>
						<input type="text" name="titulo" id="titulo" class="form-control">
					</div>
					<div class="form-group">
						<label for="año">Año</label>
						<input type="text" name="año" id="año" class="form-control">
					</div>
					<div class="form-group">
						<label for="duracion">Duracion</label>
						<input type="text" name="duracion" id="duracion" class="form-control">
					</div>
					<div class="form-group">
						<label for="pais">Pais</label>
						<input type="text" name="pais" id="pais" class="form-control">
					</div>
					<div class="form-group">
						<label for="direccion">Direccion</label>
						<input type="text" name="direccion" id="direccion" class="form-control">
					</div>
					<div class="form-group">
						<label for="guion">guion</label>
						<input type="text" name="guion" id="guion" class="form-control">
					</div>
					<div class="form-group">
						<label for="musica">Musica</label>
						<input type="text" name="musica" id="musica" class="form-control">
					</div>
					<div class="form-group">
						<label for="fotografia">Fotografia</label>
						<input type="text" name="fotografia" id="fotografia" class="form-control">
					</div>
					<div class="form-group">
						<label for="reparto">Reparto</label>
						<textarea name="reparto" id="reparto" class="form-control" rows="3">

						</textarea>
					</div>
					<div class="form-group">
						<label for="productora">Productora</label>
						<input type="text" name="productora" id="productora" class="form-control">
					</div>
					<div class="form-group">
						<label for="genero">Genero</label>
						<input type="text" name="genero" id="genero" class="form-control">
					</div>
					<div class="form-group">
						<label for="sinopsis">Sinopsis</label>
						<textarea name="sinopsis" id="sinopsis" class="form-control" rows="3">

						</textarea>
					</div>
					<div class="form-group">
						<label for="foto">Foto</label>
						<input type="file" name="foto" id="foto" class="form-control">
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir pelicula</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection