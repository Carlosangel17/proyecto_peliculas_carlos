@extends("layouts.master")

@section("titulo")
	Info pelicula
@endsection
@section("contenido")

	<div class="row">
		<div class="col-sm-4">{{--TODO: Imagen de la pelicula--}}
			<img src="{{ asset('assets/images') }}/{{ $pelicula->foto}}" class="img-fluid" style="height:400px">
		</div>
		<div class="col-xs-12 col-sm-6">
			{{--TODO: Datos de la pelicula--}}
			<table class="tabla_mostrar">
				
					<tr>
						<td>
							Titulo:
						</td>
						<td>
							{{$pelicula->titulo}}.
						</td>
					</tr>
					<tr>
						<td>
							Año:
						</td>
						<td>
							{{$pelicula->year}} 
						</td>
					</tr>
					<tr>
						<td>
							duracion:
						</td>
						<td>
							{{$pelicula->duracion}} min.
						</td>
					</tr>
					<tr>
						<td>
							pais:
						</td>
						<td>
							{{$pelicula->pais}}.
						</td>
					</tr>
					<tr>
						<td>
							Direccion:
						</td>
						<td>
							{{$pelicula->direccion}}.
						</td>
					</tr>
					<tr>
						<td>
							Guion:
						</td>
						<td>
							{{$pelicula->guion}}.
						</td>
					</tr>
					<tr>
						<td>
							Musica:
						</td>
						<td>
							{{$pelicula->musica}}.
						</td>
					</tr>
					<tr>
						<td>
							Fotografia:
						</td>
						<td>
							{{$pelicula->fotografia}}.
						</td>
					</tr>
					<tr>
						<td>
							Reparto:
						</td>
						<td>
							{{$pelicula->reparto}}.
						</td>
					</tr>
					<tr>
						<td>
							Productora:
						</td>
						<td>
							{{$pelicula->productora}}.
						</td>
					</tr>
					<tr>
						<td>
							Genero:
						</td>
						<td>
							{{$pelicula->genero}}.
						</td>
					</tr>
					<tr>
						<td>
							Sinopsis:
						</td>
						<td>
							{{$pelicula->sinopsis}}.
						</td>
					</tr>
				<tr>
					<td>
						<a href="{{ url('peliculas/crearResena') }}/{{$pelicula->id}}" class="btn btn-success" role="button">Añadir reseña</a>
					</td>
					<td>
						<a href="{{ url('peliculas/resenas') }}/{{$pelicula->id}}" class="btn btn-success" role="button">Reseñas</a>
					</td>
					<td>
						<a href="{{ url('/peliculas') }}" class="btn btn-info" role="button">Volver al listado</a>
					</td>
				</tr>
			</table>

		</div>
		
	</div>
@endsection