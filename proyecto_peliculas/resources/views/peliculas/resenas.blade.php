@extends("layouts.master")

@section("titulo")
	Info pelicula
@endsection
@section("contenido")

	<div class="row">
		<div class="col-sm-4">{{--TODO: Imagen de la pelicula--}}
			<img src="{{ asset('assets/images') }}/{{ $pelicula->foto}}" class="img-fluid" style="height:400px">
		</div>
		<div class="col-xs-12 col-sm-6">
			{{--TODO: Datos de la pelicula--}}
			<table class="tabla_mostrar">
				
				<tr>
					<td>
						{{$pelicula->titulo}}.
					</td>
				</tr>
				@foreach($pelicula->getResenas as $resena)
					<tr>
						<td>
							{{$resena->critica}}.
						</td>
						<td>
							Nota: {{$resena->valoracion}}.
						</td>
					</tr>
				@endforeach 
					
				<tr>
					<td>
						<a href="{{ url('peliculas/crearResena') }}/{{$pelicula->id}}" class="btn btn-success" role="button">Añadir reseña</a>
					</td>
					<td>
						<a href="{{ url('/peliculas') }}" class="btn btn-info" role="button">Volver al listado</a>
					</td>
				</tr>
			</table>

		</div>
		
	</div>
@endsection