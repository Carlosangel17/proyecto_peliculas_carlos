@extends("layouts.master")


@section("titulo")
	Peliculas
@endsection
@section("contenido")

<div class="row">
	@foreach( $peliculas as $pelicula)
		<div class="col-xs-12 col-sm-6 col-md-4 cajas_mascota" >
			<a href="{{ url('/peliculas/informacion/' . $pelicula->id) }}">
				<img src="{{ asset('assets/images') }}/{{ $pelicula->foto}}" class="img-fluid" style="height:200px"/>
				<h4 style="min-height:45px;margin:5px 0 10px 0">{{$pelicula->titulo}}
				</h4>
			</a>
		</div>
	@endforeach
</div>
@endsection