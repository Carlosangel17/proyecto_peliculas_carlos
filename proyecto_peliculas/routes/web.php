<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "InicioController@getInicio");

Route::get('peliculas', "PeliculasController@getPeliculas");

Route::get('peliculas/informacion/{id}', "PeliculasController@getInformacion");

Route::get('peliculas/crear', "PeliculasController@getCrear");

Route::post('peliculas/crear', "PeliculasController@postCrear");

Route::get('peliculas/crearResena/{id}', "PeliculasController@getCrearResena");

Route::post('peliculas/crearResena/{id}', "PeliculasController@postCrearResena");

Route::get('peliculas/resenas/{id}', "PeliculasController@getResenas");
